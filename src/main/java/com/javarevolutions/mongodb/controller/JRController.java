package com.javarevolutions.mongodb.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.javarevolutions.mongodb.entity.Entidad;
import com.javarevolutions.mongodb.service.JRService;

@RestController
@RequestMapping("/mongodb")
public class JRController {

	@Autowired
	JRService serviceJR;

	private Entidad entidad;

	@GetMapping("/messages")
	public ResponseEntity<List<Entidad>> getMessages() {
		return ResponseEntity.ok(serviceJR.findAll());
	}

	/**
	 *
	 * @param id
	 * @return
	 */
	@GetMapping("/messages/{id}")
	public ResponseEntity<Entidad> getMessageById(@PathVariable String id) {
		Optional<Entidad> entidad = serviceJR.findOne(id);
		if(entidad.isPresent()){
			return ResponseEntity.of(entidad);
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@PostMapping("/messages")
	public ResponseEntity<Entidad> postMessages(@RequestBody Entidad entity) {
		return ResponseEntity.ok(serviceJR.save(entity));
	}

	@PutMapping("/messages")
	public ResponseEntity<Entidad> putMessages(@RequestBody Entidad entity) {
		serviceJR.update(entity);
		return ResponseEntity.noContent().build();
	}

	@DeleteMapping("/messages/{id}")
	public ResponseEntity<Void> deleteMessages(@PathVariable String id) {
		serviceJR.delete(id);
		return ResponseEntity.noContent().build();
	}

}
