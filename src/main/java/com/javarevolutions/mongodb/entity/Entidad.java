package com.javarevolutions.mongodb.entity;

import java.io.Serializable;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


// @Document para definir un nombre de una colección cuando el objeto se guarde en MongoDB.
// En este caso, cuando el objeto “Entidad” se guarde, se hará dentro de la colección “message”.
@Document(collection = "message")
public class Entidad implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@NotNull
	private String id;
	private String mensaje;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
}
