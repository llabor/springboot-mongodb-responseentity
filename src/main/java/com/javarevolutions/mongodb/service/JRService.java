package com.javarevolutions.mongodb.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.javarevolutions.mongodb.entity.Entidad;
import com.javarevolutions.mongodb.repository.JRRepository;

@Service
public class JRService {

	@Autowired
	JRRepository repository;

	private final MongoOperations mongoOperations;

	@Autowired
	public JRService(MongoOperations mongoOperations) {
		this.mongoOperations = mongoOperations;
	}

	public List<Entidad>findAll() {
		return repository.findAll();
	}

	public Optional<Entidad> findOne(String id) {
		Entidad d = this.mongoOperations.findOne(new Query(Criteria.where("id").is(id)), Entidad.class);
		//Optional<Entidad> entidad = Optional.ofNullable(d);
		return Optional.ofNullable(d);
	}

	public Entidad save(Entidad entity) {
		this.mongoOperations.save(entity);
		return findOne(entity.getId()).get();
	}

	public void update(Entidad entity) {
		this.mongoOperations.save(entity);
	}

	public void delete(String id) {
		repository.deleteById(id);
		//this.mongoOperations.findAndRemove(new Query(Criteria.where("id").is(id)), Entidad.class);
	}
}
//	public Entidad consultarPorId(String id) {
//		Optional<Entidad> entidad = repository.findById(id);
//		if (entidad.isPresent()) {
//			return entidad.get();
//		} else
//			throw new RuntimeException();
//	}

